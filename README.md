#Stream Scripts

These Files are used in my streams to perform various tasks. Some have prerequisite dependencies.

Most are triggered using StreamController,

https://streamcontroller.github.io/docs/latest/

which is avaliable on FlatHub here:

https://flathub.org/apps/com.core447.StreamController

I used to run streamdeck_ui instead, but it broke, and this new
app supports importing old ui .json configs. But it does not support exporting them, so therefore the previous backup i had here isnt current (you can still find it in old commits)

Ive since switched to their OBS plugins. However i still
find these scripts useful as the plugin doesn't allow for bulk control across scenes for specific items.


- `play-wav-history.sh`
   - This file is designed to work like bash history - run it, and specify a number - smaller numbers play more recent WAV files
   - Folders to search are in a folder defined by the variable `WAV_HIST_FILES`
   - While it is defined to work like bash history - it actually just uses reverse alphabetical ordering as it expects all files to be timestamps
   - Requires `pipewire-audio` from package repo

- `toggleAllScenesItem.sh`
   - This file is for toggling Scene Sources In obs, it takes in the Source name as a parameter, and iterate through all scenes and toggle the source
   - scenes are defined by the `scenes` array at the top of the file
   - Requires `obs-cmd` from AUR (github: https://github.com/grigio/obs-cmd)
