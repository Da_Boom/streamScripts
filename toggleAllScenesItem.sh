#!/bin/bash


scenes=('STARTING_SOON' 'FaceCam' 'This-PC' 'Desktop-TLCam'
	'Desktop-TRCam' 'Desktop-BLCam' 'Desktop-BRCam' 'C922')

echo Toggling Scene Item $1
for scene in "${scenes[@]}"; do
    echo Current scene $scene
    obs-cmd scene-item toggle $scene $1
done
