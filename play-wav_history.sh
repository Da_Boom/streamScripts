#!/bin/bash

WAV_HIST_FILES=(/mnt/DATA_LINUX/JACK-timemachine/*)
AUDIO_SINK=SBOARD

echo ORDINARY
printf "[%s]\n" "${WAV_HIST_FILES[@]}"

IFS=$'\n' sorted=($(sort -r <<<"${WAV_HIST_FILES[*]}")); unset IFS


echo REVERSED
printf "[%s]\n" "${sorted[@]}"

echo "Attempting to play  ${sorted[$1]}"

pw-play ${sorted[$1]} --target=$AUDIO_SINK --latency=1ns

